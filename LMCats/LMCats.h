//  LMCats.h
//  Created by Louis Moga

#import "UITextView+LMAdditions.h"
#import "NSString+LMAdditions.h"
#import "UITextField+LMAdditions.h"
#import "NSDictionary+LMAdditions.h"
#import "NSDate+LMAdditions.h"
#import "UILabel+LMAdditions.h"
#import "UIView+LMAdditions.h"
#import "UIImage+LMAdditions.h"
#import "UIButton+LMAdditions.h"
#import "UITapGestureRecognizer+LMAdditions.h"
#import "UIAlertController+LMAdditions.h"
#import "UIViewController+LMAdditions.h"
#import "UIColor+LMAdditions.h"
#import "NSMutableDictionary+LMAdditions.h"
#import "UISwipeGestureRecognizer+LMAdditions.h"



#ifndef LMBootstrap_h
#define LMBootstrap_h


#define klmColorWhite           [UIColor whiteColor]
#define klmColorBlack           [UIColor blackColor]
#define klmColorRed             [UIColor redColor]
#define klmColorYellow          [UIColor yellowColor]
#define klmColorGreen           [UIColor greenColor]
#define klmColorBlue            [UIColor blueColor]
#define klmColorCyan            [UIColor cyanColor]
#define klmColorOrange          [UIColor orangeColor]
#define klmColorWhite           [UIColor whiteColor]
#define klmColorClearC          [UIColor clearColor]


#define NI(x) [NSNumber numberWithInt: x]
#define ND(x) [NSNumber numberWithDouble: x]
#define NB(x) [NSNumber numberWithBool: x]
#define NF(x) [NSNumber numberWithFloat: x]


#define SYSTEM_VERSION_LESS_THAN(v)         ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define LLocal(v)                           [[NSBundle mainBundle] localizedStringForKey:(v) value:@"" table:nil]

#define form NSString stringWithFormat:

#define lmSharedApp             [UIApplication sharedApplication]
#define lmScreenBounds          [[UIScreen mainScreen] bounds]
#define lmNoteCenter            [NSNotificationCenter defaultCenter]

#define klmColorLinkBlue        [UIColor colorWithRed:0 green:0.478431373 blue:1 alpha:1]
#define klmColorGreenBack        [UIColor colorWithRed:0.721568627 green:0.850980392 blue:0.545098039 alpha:1]
#define klmColorGrayBack        [UIColor colorWithRed:0.825 green:0.825 blue:0.825 alpha:1]

#define klmPrefFirstTimeLaunch              @"KLMFTL"
#define klmPrefInstanceID                   @"KLMINSTANCEID"





#endif /* LMBootstrap_h */
