//  NSMutableDictionary+LMAdditions.m
//  Created by Louis Moga

#import "NSMutableDictionary+LMAdditions.h"

@implementation NSMutableDictionary (LMAdditions)


- (void) setInt:(int)intValue forKey:(nonnull id<NSCopying>)keyName {
    [self setObject:[NSNumber numberWithInt:intValue] forKey:keyName];
}

- (int) intForKey:(nonnull id)keyName {
    NSNumber *number = [self objectForKey:keyName];
    if (!number || ![number isKindOfClass:[NSNumber class]]) return 0;
    return [number intValue];
}

@end
