//  LLog.h
//
//  Created by Louis Moga on 9/12/12.
//  Copyright © 2012-2016 Louis Moga, All rights reserved.

#import <Foundation/Foundation.h>

#define LLog(fmt, ...) [[LLog sharedInstance] log:fmt, ##__VA_ARGS__]
#define e(exception) [LLog e:exception]

@interface LLog : NSObject
{
    NSFileHandle *logFile;
    NSString *(^sanitizeLogOutputBlock) (NSString *message);
    NSMutableDictionary *sanitizers;
    NSMutableDictionary *obfuscators;
    NSString *obfuscationKey;
}

+ (LLog *)sharedInstance;
- (void)log:(NSString *)format, ...;

+ (void)printAppStartingInfo;
- (void)setSanitizeBlock:(NSString *(^) (NSString *message))logSanitizeBlock;
+ (NSString *) instanceID;

+ (NSString *)paddedForHeader:(NSString *)padMe PadTo:(int)padTo;

- (NSData *)debugFileData;

+ (void) e:(NSException *) exception;
+ (void) e:(NSException *) exception header:(NSString *)header;

@property (nonatomic, retain) NSMutableDictionary *sanitizers;
@property (nonatomic, retain) NSMutableDictionary *obfuscators;
@property (nonatomic, retain) NSString *obfuscationKey;

@end

