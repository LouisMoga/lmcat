//  UILabel+UIAdditions.m
//  Created by Louis Moga.

#import "UILabel+LMAdditions.h"
#import "NSString+LMAdditions.h"
#import "UIView+LMAdditions.h"

@implementation UILabel (LMAdditions)

- (void)setTextWithFormat:(NSString *)format, ... {
    va_list args;
    va_start(args, format);
    format = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    
    [self setText:format];
}

- (CGSize)lmSizeForText {
    return [self.text lmSizeInFont:self.font];
}

- (float)lmNextLineY {
    return self.frame.origin.y + [self lmSizeForText].height * 1;
}

- (void)lmFixHeight {
    [self lmSetHeight:[self lmSizeForText].height];
}

- (double)lmTextBottom {
    return self.lmFrameTop + self.lmHeight / 2 + self.lmSizeForText.height / 2;
}

- (void)lmAddChildBelow:(NSString *)setText {
    UILabel *childLabel = [[UILabel alloc] init];
    
    [childLabel setFont:[UIFont fontWithName:self.font.fontName size:self.font.pointSize - 4]];
    [childLabel setFrame:CGRectMake(self.lmFrameLeft, self.lmTextBottom, self.lmSizeForText.width, 20)];
    
    [childLabel setText:setText];
    [childLabel setTextColor:self.textColor];
    [childLabel setBackgroundColor:self.backgroundColor];
    [childLabel setTextAlignment:NSTextAlignmentCenter];
    
    [childLabel lmFixHeight];
    
    [self.superview addSubview:childLabel];
}

@end
