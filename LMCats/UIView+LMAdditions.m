//  UIView+LMAdditions.m
//  Created by Louis Moga

#import "UIView+LMAdditions.h"
#import <objc/runtime.h>
#import "LMCats.h"
#import "LLog.h"


@implementation UIView (LMAdditions)
@dynamic lmTag2, lmTagS1;



- (void) addBackgroundImage:(UIImage *)backgroundImage {
    UIImageView *backgroundImageView = [[UIImageView alloc] init];
    [backgroundImageView            setImage:backgroundImage];
    [backgroundImageView      setContentMode: UIViewContentModeScaleAspectFill];
    [backgroundImageView setTranslatesAutoresizingMaskIntoConstraints:NO];

    [self insertSubview:backgroundImageView atIndex:0];
    
    [self addConstraint:[NSLayoutConstraint
                           constraintWithItem: self
                           attribute:NSLayoutAttributeLeading
                           relatedBy:NSLayoutRelationEqual
                           toItem:backgroundImageView
                           attribute:NSLayoutAttributeLeading
                           multiplier:1
                           constant:0]];
    [self addConstraint:[NSLayoutConstraint
                           constraintWithItem: self
                           attribute:NSLayoutAttributeTrailing
                           relatedBy:NSLayoutRelationEqual
                           toItem:backgroundImageView
                           attribute:NSLayoutAttributeTrailing
                           multiplier:1
                           constant:0]];
    [self addConstraint:[NSLayoutConstraint
                           constraintWithItem: self
                           attribute:NSLayoutAttributeTop
                           relatedBy:NSLayoutRelationEqual
                           toItem:backgroundImageView
                           attribute:NSLayoutAttributeTop
                           multiplier:1
                           constant:0]];
    [self addConstraint:[NSLayoutConstraint
                           constraintWithItem: self
                           attribute:NSLayoutAttributeBottom
                           relatedBy:NSLayoutRelationEqual
                           toItem:backgroundImageView
                           attribute:NSLayoutAttributeBottom
                           multiplier:1
                           constant:0]];
}

//static BOOL AutoresizingMaskOverriden = true;

- (void)removeAllSubviews {
    for (long i = self.subviews.count - 1; i >= 0; i--) {

        for (long ii = self.constraints.count - 1; ii >= 0; ii--) {
            NSLayoutConstraint *cont = self.constraints[ii];
            
            if ([cont.firstItem isEqual:self.subviews[i]]) {
                [self removeConstraint:cont];
            }
        }

        [self.subviews[i] removeFromSuperview];
    }
}




//All views with a tag, extending default method
- (NSArray<UIView *> *)viewsWithTag:(int)Tag {
    NSMutableArray *returnViews = [[NSMutableArray alloc] init];
    
    for (UIView *view in self.subviews) {
        if (view.tag == Tag) [returnViews addObject:view];
        
        [returnViews addObjectsFromArray:[view viewsWithTag:Tag]];
    }
    
    return returnViews;
}



//Tag, LMTag2 - Single View
- (UIView *)viewWithTag:(int)Tag LMTag2:(int)LMTag2 {
    NSArray *returnViews = [self viewsWithTag:Tag LMTag2:LMTag2];
    
    if (returnViews && returnViews.count >= 1) {
        return [returnViews objectAtIndex:0];
    } else {
        return nil;
    }
}

//Tag, LMTag2 - Views
- (NSArray<UIView *> *)viewsWithTag:(int)Tag LMTag2:(int)LMTag2 {
    NSMutableArray *returnViews = [[NSMutableArray alloc] init];
    
    for (UIView *view in self.subviews) {
        
        if (view.tag == Tag && [view.lmTag2 intValue] == LMTag2) {
            [returnViews addObject:view];
        }
        
        [returnViews addObjectsFromArray:[view viewsWithTag:Tag LMTag2:LMTag2]];
    }
    
    return returnViews;
}




//Tag, LMTagID - Single View
- (UIView *)viewWithTag:(int)Tag LMTagID:(int)LMTagID {
    NSArray *returnViews = [self viewsWithTag:Tag LMTagID:LMTagID];
    
    if (returnViews && returnViews.count >= 1) {
        return [returnViews objectAtIndex:0];
    } else {
        return nil;
    }
}

//Tag, LMTagID - Views
- (NSArray<UIView *> *)viewsWithTag:(int)Tag LMTagID:(int)LMTagID {
    NSMutableArray *returnViews = [[NSMutableArray alloc] init];
    
    for (UIView *view in self.subviews) {
        
        if (view.tag == Tag && view.lmTagID == LMTagID) {
            [returnViews addObject:view];
        }
        
        [returnViews addObjectsFromArray:[view viewsWithTag:Tag LMTagID:LMTagID]];
    }
    
    return returnViews;
}



//Tag, LMTagID, LMTag2 - Single View
- (UIView *)viewWithTag:(int)Tag LMTagID:(int)LMTagID LMTag2:(int)LMTag2 {
    NSArray *returnViews = [self viewsWithTag:Tag LMTagID:LMTagID LMTag2:(int)LMTag2];
    
    if (returnViews && returnViews.count >= 1) {
        return [returnViews objectAtIndex:0];
    } else {
        return nil;
    }
}

//Tag, LMTagID, LMTag2 - Views
- (NSArray<UIView *> *)viewsWithTag:(int)Tag LMTagID:(int)LMTagID LMTag2:(int)LMTag2 {
    NSMutableArray *returnViews = [[NSMutableArray alloc] init];
    
    for (UIView *view in self.subviews) {
        if (view.tag == Tag && view.lmTagID == LMTagID && view.lmTag2.intValue == LMTag2) {
            [returnViews addObject:view];
        }
        
        [returnViews addObjectsFromArray:[view viewsWithTag:Tag LMTagID:LMTagID LMTag2:LMTag2]];
    }
    
    return returnViews;
}






//-(BOOL)translatesAutoresizingMaskIntoConstraints {
//    LLog(@"Ret: %i", AutoresizingMaskOverriden);
//    
//    if ([self.superview isKindOfClass:[UIWindow class]])
//        return true;
//    else
//        return AutoresizingMaskOverriden;
//}
//
//-(void)setTranslatesAutoresizingMaskIntoConstraints:(BOOL)translatesAutoresizingMaskIntoConstraints {
//    AutoresizingMaskOverriden = translatesAutoresizingMaskIntoConstraints;
//}

- (UIGestureRecognizer *)addGestBlock:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location)) blockToRun {
    UITapGestureRecognizer *gest = [[UITapGestureRecognizer alloc] initWithHandler:blockToRun];

    [self addGestureRecognizer:gest];
    [self setUserInteractionEnabled:YES];

    return gest;
}


- (UIGestureRecognizer *)addGest:(id)target Action:(SEL)Selector {
    UIGestureRecognizer *gest = [[UITapGestureRecognizer alloc] initWithTarget:target action:Selector];

    [self addGestureRecognizer:gest];
    [self setUserInteractionEnabled:YES];
    
    return gest;
}

- (float)lmFrameBottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (float)lmFrameRight {
    return self.frame.origin.x + self.frame.size.width;
}

- (float)lmFrameTop {
    return self.frame.origin.y;
}

- (float)lmFrameLeft {
    return self.frame.origin.x;
}

- (float)lmWidth {
    return self.frame.size.width;
}

- (float)lmHeight {
    return self.frame.size.height;
}

- (void)lmSetHeight:(double)height {
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height)];
}
- (void)lmSetWidth:(double)width {
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, width, self.frame.size.height)];
}
- (void)lmAdjustHeight:(double)height {
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height + height)];
}
- (void)lmAdjustWidth:(double)width {
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width + width, self.frame.size.height)];
}
- (void)lmSetX:(double)x {
    [self setFrame:CGRectMake(x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
}
- (void)lmSetY:(double)y {
    [self setFrame:CGRectMake(self.frame.origin.x, y, self.frame.size.width, self.frame.size.height)];
}
- (void)lmAdjustX:(double)x {
    [self setFrame:CGRectMake(self.frame.origin.x + x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
}
- (void)lmAdjustY:(double)y {
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y + y, self.frame.size.width, self.frame.size.height)];
}

- (void)setLmTag2:(NSNumber *)_lmTag2 {
    objc_setAssociatedObject(self, @selector(lmTag2), _lmTag2, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)lmTag2 {
    NSNumber *returnVal = objc_getAssociatedObject(self, @selector(lmTag2));
    return returnVal ? returnVal : [NSNumber numberWithInt:0];
}



- (void)setLmTagID:(int)_lmTagID {
    NSNumber *intVersion = [NSNumber numberWithInt:_lmTagID];
    objc_setAssociatedObject(self, @selector(lmTagID), intVersion, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (int)lmTagID {
    NSNumber *returnVal = objc_getAssociatedObject(self, @selector(lmTagID));
    return returnVal ? [returnVal intValue] : 0;
}




- (NSMutableArray *)LMData {
    NSMutableArray *result = objc_getAssociatedObject(self, @selector(LMData));
    if (result == nil) {
        result = [[NSMutableArray alloc] init];
        objc_setAssociatedObject(self, @selector(LMData), result, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return result;
}




- (void)setLmTagS1:(NSString *)_lmTagS1 {
    objc_setAssociatedObject(self, @selector(lmTagS1), _lmTagS1, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)lmTagS1 {
    NSString *returnVal = objc_getAssociatedObject(self, @selector(lmTagS1));
    return returnVal;
}

- (void)setLmTagR1:(id)_refID {
    objc_setAssociatedObject(self, @selector(lmTagR1), _refID, OBJC_ASSOCIATION_ASSIGN);
}

- (id)lmTagR1 {
    NSObject *returnVal = objc_getAssociatedObject(self, @selector(lmTagR1));
    return returnVal;
}

- (void)lmExpandWidth:(double)howMuch {
    [self lmAdjustWidth:howMuch];
    [self lmAdjustX:-howMuch / 2];
}

- (void)lmAddBorder:(double)width BorderColor:(UIColor *)borderColor {
    [self.layer setBorderWidth:width];
    [self.layer setBorderColor:borderColor.CGColor];
}

- (void)lmAddBorder {
    [self lmAddBorder:0.5 BorderColor:[UIColor colorWithWhite:0.5 alpha:1]];
}

- (void)lmAddBorderTop {
    [self lmAddBorderTop:klmColorBlack Thickness:1.0f];
}
- (void)lmAddBorderBottom {
    [self lmAddBorderBottom:klmColorBlack Thickness:1.0f];
}
- (void)lmAddBorderLeft {
    [self lmAddBorderLeft:klmColorBlack Thickness:1.0f];
}
- (void)lmAddBorderRight {
    [self lmAddBorderRight:klmColorBlack Thickness:1.0f];
}

- (void)lmAddBorderTop:(UIColor *)borderColor Thickness:(float)Thickness {
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [borderColor CGColor];
    upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), Thickness);
    [self.layer addSublayer:upperBorder];
}

- (void)lmAddBorderBottom:(UIColor *)borderColor Thickness:(float)Thickness {
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [borderColor CGColor];
    upperBorder.frame = CGRectMake(0, CGRectGetHeight(self.frame) - Thickness, CGRectGetWidth(self.frame), Thickness);
    [self.layer addSublayer:upperBorder];
}
- (void)lmAddBorderLeft:(UIColor *)borderColor Thickness:(float)Thickness {
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [borderColor CGColor];
    upperBorder.frame = CGRectMake(0, 0, Thickness, CGRectGetHeight(self.frame));
    [self.layer addSublayer:upperBorder];
}
- (void)lmAddBorderRight:(UIColor *)borderColor Thickness:(float)Thickness {
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [borderColor CGColor];
    upperBorder.frame = CGRectMake(CGRectGetWidth(self.frame) - Thickness, 0, Thickness, CGRectGetHeight(self.frame));
    [self.layer addSublayer:upperBorder];
}




- (void)setAlertWindow:(UIWindow *)window {
    objc_setAssociatedObject(self, @selector(getAlertWindow), window, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIWindow *)getAlertWindow {
    UIWindow *object = objc_getAssociatedObject(self, @selector(getAlertWindow));
    return object;
}



- (void)showWorkingView {
    [self showWorkingViewWithText:nil];
}

- (void)showWorkingViewWithText:(NSString *)showText {
    [self showWorkingViewWithText:showText backColor:[UIColor lightGrayColor]];
}

- (void)showWorkingViewWithText:(NSString *)showText backColor:(UIColor *)backColor {
    if ([NSThread isMainThread]) {
        [self _showWorkingViewWithText:showText backColor:backColor];
    } else {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _showWorkingViewWithText:showText backColor:backColor];
            
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
}

- (void)_showWorkingViewWithText:(NSString *)showText backColor:(UIColor *)backColor {
    LLog([@"Showing Working View" paddedForHeader:40]);
    if (![self getAlertWindow]) {
        [self setAlertWindow:[[UIWindow alloc] init]];
        [[self getAlertWindow] setWindowLevel:UIWindowLevelAlert];
    }
    
    UIView *LoadingView = nil;
    UIView *LoadingLabelCont;
    UILabel *LoadingLabel;
    UIActivityIndicatorView *spinner;
    CGRect screenRect = lmScreenBounds;
    UIWindow *alertWindow = [self getAlertWindow];
    
    [alertWindow setFrame:screenRect];
    [alertWindow setOpaque:NO];
    [alertWindow setBackgroundColor:klmColorClearC];
    
    
    LoadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, screenRect.size.height)];
    [LoadingView setTag:1];
    [LoadingView setBackgroundColor:[UIColor colorWithWhite:0.61 alpha:0.39]];
    [alertWindow addSubview:LoadingView];
    
    LoadingLabelCont = [[UIView alloc] initWithFrame:CGRectMake(0, screenRect.size.height + 40, screenRect.size.width, 50)];
    [LoadingLabelCont setBackgroundColor:backColor];
    [LoadingLabelCont.layer setCornerRadius:10];
    [LoadingLabelCont setTag:2];
    [LoadingLabelCont setClipsToBounds:YES];
    [LoadingLabelCont.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    [LoadingLabelCont.layer setBorderWidth:0.5];
    [alertWindow addSubview:LoadingLabelCont];
    
    LoadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, LoadingLabelCont.frame.size.width, 30)];
    [LoadingLabel setTextAlignment:NSTextAlignmentCenter];
    [LoadingLabel setTextColor:[UIColor blackColor]];
    [LoadingLabel setTag:3];
    [LoadingLabel setText:nil];
    [LoadingLabel setFont:[UIFont systemFontOfSize:18]];
    [LoadingLabelCont addSubview:LoadingLabel];
    
    spinner = [[UIActivityIndicatorView alloc] init];
    [spinner setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner setFrame:CGRectMake(screenRect.size.width / 2 - spinner.frame.size.width / 2, screenRect.size.height / 2 - spinner.frame.size.height / 2, spinner.frame.size.width, spinner.frame.size.height)];
    [spinner setTag:4];
    [spinner startAnimating];
    [alertWindow addSubview:spinner];
    
    
    [LoadingLabelCont setHidden:!showText];
    
    if (showText) {
        if (LoadingLabel.text) {
            [UIView animateWithDuration:.1 animations:^{
                [LoadingLabel setFrame:CGRectMake(0, -20, LoadingLabelCont.frame.size.width, 30)];
                [LoadingLabel setAlpha:0];
            } completion:^(BOOL finished) {
                [LoadingLabel setText:showText];
                [LoadingLabel setFrame:CGRectMake(0, 35, LoadingLabelCont.frame.size.width, 30)];
                [UIView animateWithDuration:.1 animations:^{
                    [LoadingLabel setFrame:CGRectMake(0, 5, LoadingLabelCont.frame.size.width, 30)];
                    [LoadingLabel setAlpha:1];
                }];
            }];
        } else {
            [LoadingLabel setText:showText];
            [LoadingLabel setAlpha:0];
            
            [UIView animateWithDuration:.2 animations:^{
                [LoadingLabelCont setFrame:CGRectMake(0, screenRect.size.height - 40, screenRect.size.width, 50)];
                [LoadingLabel setAlpha:1];
                [LoadingLabel setFrame:CGRectMake(0, 5, LoadingLabelCont.frame.size.width, 30)];
            }];
        }
        
    } else {
        [LoadingLabel setText:nil];
        
    }
    
    [alertWindow setHidden:false];
}



- (void)removeWorkingViewSynchronous {
    if (![self getAlertWindow]) return;
    LLog([@"Removing Working View Sync" paddedForHeader:40]);
    
    UIView *LoadingView =       [[self getAlertWindow] viewWithTag:1];
    UIView *LoadingLabelCont =  [[self getAlertWindow] viewWithTag:2];
    UILabel *LoadingLabel =     [[self getAlertWindow] viewWithTag:3];
    UIActivityIndicatorView *spinner = [[self getAlertWindow] viewWithTag:4];
    
    [spinner removeFromSuperview];
    [LoadingLabel removeFromSuperview];
    [LoadingLabelCont removeFromSuperview];
    [LoadingView removeFromSuperview];
    
    [[self getAlertWindow] removeFromSuperview];
    [self setAlertWindow:nil];
}


- (void)removeWorkingView {
    if (![self getAlertWindow]) return;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            LLog([@"Removing Working View Async" paddedForHeader:40]);
            [self removeWorkingViewSynchronous];
        });
    });
}

@end

