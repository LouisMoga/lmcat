//
//  UIColor+UIColor_LMAdditions.m
//  LMCats
//
//  Created by Louis Moga on 1/9/17.
//  Copyright © 2017 Louis Moga. All rights reserved.
//

#import "UIColor+LMAdditions.h"

@implementation UIColor (LMAdditions)


- (UIColor *)lighterColor
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:MIN(b * 1.3, 1.0)
                               alpha:a];
    return nil;
}


- (UIColor *)darkerColor
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:b * 0.75
                               alpha:a];
    return nil;
}

@end
