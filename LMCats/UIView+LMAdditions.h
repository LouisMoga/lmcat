//  UIView+LMAdditions.h
//  Created by Louis Moga

#import <UIKit/UIKit.h>

@interface UIView (LMAdditions)

@property(nonatomic) int lmTagID;
@property(nonatomic) NSNumber *lmTag2;
@property(nonatomic) NSString *lmTagS1;

@property (nonatomic, readonly) NSMutableArray *LMData;

- (void)removeAllSubviews;

- (void) addBackgroundImage:(UIImage *)backgroundImage;

- (float)lmFrameBottom;
- (float)lmFrameRight;
- (float)lmFrameTop;
- (float)lmFrameLeft;
- (float)lmWidth;
- (float)lmHeight;

- (void)lmAddBorderTop;
- (void)lmAddBorderBottom;
- (void)lmAddBorderLeft;
- (void)lmAddBorderRight;
- (void)lmAddBorderTop:(UIColor *)borderColor Thickness:(float)Thickness;
- (void)lmAddBorderBottom:(UIColor *)borderColor Thickness:(float)Thickness;
- (void)lmAddBorderLeft:(UIColor *)borderColor Thickness:(float)Thickness;
- (void)lmAddBorderRight:(UIColor *)borderColor Thickness:(float)Thickness;


- (void)lmSetHeight:(double)height;
- (void)lmSetWidth:(double)width;
- (void)lmSetX:(double)x;
- (void)lmSetY:(double)y;

- (NSArray<UIView *> *)viewsWithTag:(int)Tag;

- (UIView *)viewWithTag:(int)Tag LMTag2:(int)LMTag2;
- (NSArray<UIView *> *)viewsWithTag:(int)Tag LMTag2:(int)LMTag2;

- (UIView *)viewWithTag:(int)Tag LMTagID:(int)LMTagID;
- (NSArray<UIView *> *)viewsWithTag:(int)Tag LMTagID:(int)LMTagID;

- (UIView *)viewWithTag:(int)Tag LMTagID:(int)LMTagID LMTag2:(int)LMTag2;
- (NSArray<UIView *> *)viewsWithTag:(int)Tag LMTagID:(int)LMTagID LMTag2:(int)LMTag2;

- (void)lmAdjustX:(double)x;
- (void)lmAdjustY:(double)y;
- (void)lmAdjustHeight:(double)height;
- (void)lmAdjustWidth:(double)width;

- (void)lmExpandWidth:(double)howMuch;

- (void)lmAddBorder:(double)width BorderColor:(UIColor *)borderColor;
- (void)lmAddBorder;

- (UIGestureRecognizer *)addGest:(id)target Action:(SEL)Selector;
- (UIGestureRecognizer *)addGestBlock:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location)) blockToRun;

- (void)showWorkingView;
- (void)showWorkingViewWithText:(NSString *)showText;
- (void)showWorkingViewWithText:(NSString *)showText backColor:(UIColor *)backColor;
- (void)removeWorkingViewSynchronous;
- (void)removeWorkingView;

- (void)setLmTagR1:(id)_refID;
- (id)lmTagR1;


@end







