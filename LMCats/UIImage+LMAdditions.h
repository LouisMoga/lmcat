//  UIImage+LMAdditions.h
//  Created by Louis Moga

#import <UIKit/UIKit.h>

@interface UIImage (LMAdditions)

- (UIImage *)fixOrientation;
+ (UIImage *)imageWithColor:(UIColor *)color RectSize:(CGRect)rect;

@end
