//  UITextView+LMAdditions.h
//  Created by Louis Moga

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UITextView (LMAdditions)

- (void)setTextWithFormat:(NSString *)format, ...; 
- (CGSize)lmSizeForText;
- (float)lmNextLineY;
- (void)lmFixHeight;
- (void)removeDefaultPadding;
- (CGSize)lmSizeForTextContainer;

- (double)lmTextBottom;


@end
