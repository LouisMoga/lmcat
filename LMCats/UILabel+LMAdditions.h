//  UILabel+UIAdditions.h
//  Created by Louis Moga

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UILabel (LMAdditions)

- (void)setTextWithFormat:(NSString *)format, ...; 
- (CGSize)lmSizeForText;
- (float)lmNextLineY;
- (void)lmFixHeight;

- (double)lmTextBottom;
- (void)lmAddChildBelow:(NSString *)setText;

@end
