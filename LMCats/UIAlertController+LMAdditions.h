//  UIAlertController+LMAdditions.h
//  Created by Louis Moga

#import <UIKit/UIKit.h>

@interface UIAlertController (LMAdditions)

- (void)show;
- (void)show:(BOOL)animated;

@end
