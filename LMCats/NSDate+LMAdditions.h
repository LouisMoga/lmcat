//  NSDate+NSAdditions.h
//  Created by Louis Moga

#import <Foundation/Foundation.h>

@interface NSDate (LMAdditions)

- (NSString *) stringFormatted:(NSString *)format;
- (bool)isBefore:(NSDate *)otherDate;
- (bool)isAfter:(NSDate *)otherDate;
- (bool)isToday;

-(NSDate *) toLocalTime;
-(NSDate *) toGlobalTime;
-(NSDate *) toUTCTime;



-(NSString *) UTCTimeString;
-(NSString *) UTCDateString;
-(NSString *) UTCDateTimeString;
-(NSString *) NiceLocalTimeString;
-(NSString *) NiceLocalDateString;
-(NSString *) NiceLocalDateTimeString;

+(NSDate *) dateFromNiceDateString:(NSString *)dateString;
+(NSDate *) dateFromNiceDateTimeString:(NSString *)dateTimeString;

-(NSString *) HoursMinutesSecondsFromTimeInterval:(NSTimeInterval)interval;

-(NSDate *) DateFromUTFJSONString:(NSString *)dateString;


- (NSDate *)dateWithZeroSeconds;
- (NSDate *)dateOnly;

@end
