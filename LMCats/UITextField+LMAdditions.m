//  UITextField+LMAdditions.m
//  Created by Louis Moga

#import "UITextField+LMAdditions.h"
#import <objc/runtime.h>

@implementation UITextField (LMAdditions)

- (void)cmUnderlineField {
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height - 1, self.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
    [self.layer addSublayer:bottomBorder];
}

- (void)addLeftView:(int)width {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 1)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}








- (void)bk_setHandler:(void (^)(UITextField *sender)) handler
{
    objc_setAssociatedObject(self, @"BKTextFieldContrelEventHandler", handler, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)setBkhandler:(void (^)(UITextField *sender)) handler
{
    objc_setAssociatedObject(self, @"BKTextFieldContrelEventHandler", handler, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(UITextField *sender))bk_handler
{
    return objc_getAssociatedObject(self, @"BKTextFieldContrelEventHandler");
}

- (void)addBlock:(void (^)(UITextField *sender)) block forControlEvents:(UIControlEvents)controlEvents
{
    if (!self) return;

    [self addTarget:self action:@selector(bk_handleAction:) forControlEvents:controlEvents];

    [self setBkhandler:block];
}


- (void)bk_handleAction:(UITextField *) _sender
{
    void (^handler)(UITextField *sender) = _sender.bk_handler;
    if (!handler) return;
    
    void (^block)(void) = ^{
        handler(self);
    };

    block();
}




@end
