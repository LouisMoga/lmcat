//
//  UISwipeGestureRecognizer+LMAdditions.m
//  LMCats
//
//  Created by Louis Work on 10/14/21.
//  Copyright © 2021 Louis Moga. All rights reserved.
//

#import "UISwipeGestureRecognizer+LMAdditions.h"
#import <objc/runtime.h>

@interface UISwipeGestureRecognizer (LMAdditions)
- (void)bk_handleAction:(UIGestureRecognizer *)recognizer;
- (void)bk_setHandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))handler;
@end

@implementation UISwipeGestureRecognizer (LMAdditions)


- (void)bk_setHandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))handler
{
    objc_setAssociatedObject(self, @"BKGestureRecognizerBlockKey", handler, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



- (void)setBkhandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))handler
{
    objc_setAssociatedObject(self, @"BKGestureRecognizerBlockKey", handler, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))bk_handler
{
    return objc_getAssociatedObject(self, @"BKGestureRecognizerBlockKey");
}




- (id)initWithHandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))block
{
    self = [self initWithTarget:self action:@selector(bk_handleAction:)];
    if (!self) return nil;

    [self setBkhandler:block];

    return self;
}


- (void)bk_handleAction:(UISwipeGestureRecognizer *)recognizer
{
    void (^handler)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) = recognizer.bk_handler;
    if (!handler) return;
    
    CGPoint location = [self locationInView:self.view];
    void (^block)(void) = ^{
        handler(self, self.state, location);
    };

    block();
}


@end
