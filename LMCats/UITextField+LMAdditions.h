//  UITextField+LMAdditions.h
//  Created by Louis Moga

#import <UIKit/UIKit.h>

@interface UITextField (LMAdditions)

- (void)cmUnderlineField;
- (void)addLeftView:(int)width;

- (void)addBlock:(void (^)(UITextField *sender)) block forControlEvents:(UIControlEvents)controlEvents;

@end
