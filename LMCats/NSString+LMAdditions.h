//  NSString+LMAdditions.h
//  Created by Louis Moga

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (LMAdditions)

- (NSString *)lmGetSHA1Hash;
- (CGFloat)lmHeightInFont:(UIFont *)theFont;
- (CGFloat)lmHeightInFont:(UIFont *)theFont inWidth:(double)theWidth;
- (CGSize)lmSizeInFont:(UIFont *)theFont;
- (CGSize)lmSizeInFont:(UIFont *)theFont inWidth:(double)theWidth;
- (BOOL)contains:(NSString *)string;
- (NSString *)lmFormat10digitsToPhoneNumber;
- (NSString *)lmStringWithURLEncoding;
- (BOOL)matchesRegex:(NSString *)regex;
- (NSString *)paddedForHeader:(int)padTo;

- (NSUInteger)occurrenceCountOfCharacter:(UniChar)character;

@end
