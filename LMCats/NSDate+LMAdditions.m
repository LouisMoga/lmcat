//  NSDate+NSAdditions.m
//  Created by Louis Moga

#import "NSDate+LMAdditions.h"

@implementation NSDate (LMAdditions)

- (bool)isToday {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    NSDate *thisDate = [cal dateFromComponents:components];
    
    return [today isEqualToDate:thisDate];
}

- (NSString *)stringFormatted:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    
    return [formatter stringFromDate:self];
}


- (bool)isBefore:(NSDate *)otherDate {
    return ([self compare:otherDate] == NSOrderedAscending);
}


- (bool)isAfter:(NSDate *)otherDate {
    return ([self compare:otherDate] == NSOrderedDescending);
}

-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toGlobalTime
{
    return [self toUTCTime];
}

-(NSDate *) toUTCTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}


+(NSDate *) dateFromNiceDateTimeString:(NSString *)dateTimeString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];

    return [dateFormatter dateFromString:dateTimeString];
}

+(NSDate *) dateFromNiceDateString:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];

    return [dateFormatter dateFromString:dateString];
}


-(NSString *) NiceLocalDateTimeString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];

    return [dateFormatter stringFromDate: self];
}

-(NSString *) NiceLocalDateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];

    return [dateFormatter stringFromDate:self];
}

-(NSString *) NiceLocalTimeString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];

    return [dateFormatter stringFromDate:self];
}


-(NSString *) UTCDateTimeString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];

    NSString *utcDateString = [dateFormatter stringFromDate:self];
    return utcDateString;
}

-(NSString *) UTCDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];

    NSString *utcDateString = [dateFormatter stringFromDate:self];
    return utcDateString;
}


-(NSString *) UTCTimeString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:self];
    //  [dateFormatter release];
    
    NSLog(@"the utc time  is %@",dateString);
    return dateString;
}


-(NSDate *) DateFromUTFJSONString:(NSString *)dateString
{
    if (![self objectNoNSNull:dateString]) {
        return nil;
    }
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    [dateFormatter setTimeZone:timeZone];
    
    dateString = [dateString stringByReplacingOccurrencesOfString:@" +0000" withString:@""];

    if ([dateString containsString:@"."]) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    } else {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    }

    NSDate *returnDate = [dateFormatter dateFromString:dateString];
    
    return (NSDate *)[self objectNoNSNull:returnDate];
}

- (NSObject *) objectNoNSNull:(NSObject *) theObject {
    if ([theObject isKindOfClass:[NSNull class]])
        return nil;
    else
        return theObject;
}



-(NSString *) HoursMinutesSecondsFromTimeInterval:(NSTimeInterval) interval {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    [dateFormatter setDateFormat:@"HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];

    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    return formattedDate;
}


- (NSDate *)dateWithZeroSeconds
{
    NSTimeInterval time = floor([self timeIntervalSinceReferenceDate] / 60.0) * 60.0;
    return  [NSDate dateWithTimeIntervalSinceReferenceDate:time];
}

- (NSDate *)dateOnly
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];

    NSString *formattedDate = [dateFormatter stringFromDate:self];

    NSDate *returnDate = [dateFormatter dateFromString:formattedDate];
    
    return returnDate;
}








@end
