//  UITextView+LMAdditions.m
//  Created by Louis Moga

#import "UITextView+LMAdditions.h"
#import "NSString+LMAdditions.h"
#import "UIView+LMAdditions.h"
#import <objc/runtime.h>

@implementation UITextView (LMAdditions)

- (void)setTextWithFormat:(NSString *)format, ... {
    va_list args;
    va_start(args, format);
    format = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    
    [self setText:format];
}

- (void)removeDefaultPadding {
    self.textContainer.lineFragmentPadding = 0;
    self.textContainerInset = UIEdgeInsetsZero;
}

- (CGSize)lmSizeForText {
    return [self.text lmSizeInFont:self.font];
}

- (CGSize)lmSizeForTextContainer {
    CGSize textBounds = [self lmSizeForText];

    textBounds.width += self.textContainerInset.left + self.textContainerInset.right;
    textBounds.height += self.textContainerInset.top + self.textContainerInset.bottom;
    
    textBounds.width += self.textContainer.lineFragmentPadding * 2;
    
    return textBounds;
}

- (float)lmNextLineY {
    return self.lmTextBottom + self.font.lineHeight * 0.25;
}

- (void)lmFixHeight {
    [self lmSetHeight:[self lmSizeForText].height];
}

- (double)lmTextBottom {
    return self.lmFrameTop + self.lmHeight / 2 + self.lmSizeForTextContainer.height / 2;
}



@end
