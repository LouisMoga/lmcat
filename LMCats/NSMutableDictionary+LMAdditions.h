//  NSMutableDictionary+LMAdditions.h
//  Created by Louis Moga

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSMutableDictionary (LMAdditions)


- (void) setInt:(int)intValue forKey:(nonnull id<NSCopying>)keyName;
- (int) intForKey:(nonnull id)keyName;

@end
