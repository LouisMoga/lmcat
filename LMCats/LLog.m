//  LLog.m
//
//  Created by Louis Moga on 9/12/12.
//  Copyright © 2012-2016 Louis Moga, All rights reserved.


#import "LLog.h"
#import "LMCats.h"
//#import "RSA.h"
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

@implementation LLog
@synthesize sanitizers, obfuscators, obfuscationKey;


- (id)init {
    self = [super init];
    sanitizeLogOutputBlock = nil;
    sanitizers = [[NSMutableDictionary alloc] init];
    obfuscators = [[NSMutableDictionary alloc] init];
    obfuscationKey = @"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtL/sAtWc7c7cDrSmEyObbrRu/+dcTxSn/VE3fguNO1vo+QTMUPcZc29mnb8W53A33gzxRTD08W7w+QDT1nBi+oLtY/Yg/R6IuIKTLRUgmHpYcpZpliOd5vd8/Lma1tnHjHXN7GsAXeBPTrZvdQG64/UE2QNbfnvECIT/gne2G/getSm35WjSOW3Z//Baytbwn3hxb8YNVc8qB+qKOA1uS9ZAMAqR8IwC3/QkJxLMCSqG+kWrpKhfYm088L0ZwtUFajqGIk0M9oB59gjBo1QP6sxkefm9a5nzvDdoe0hq+rZDOZjdW4fMyrXjSagoOPmHiRwPL6moUfJ+JCT9kU/k8QIDAQAB";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"debugging.log"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filePath])
        [fileManager createFileAtPath:filePath
                                contents:nil
                            attributes:nil];
    logFile = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
    [logFile seekToEndOfFile];
    
    return self;
}


- (NSString *)sanitizeLog:(NSString *)message {
    if (sanitizers && sanitizers.count > 0) {
        for (NSString *key in sanitizers) {
            NSString *value = [sanitizers objectForKey:key];
            
            if ([message contains:value]) {
                message = [message stringByReplacingOccurrencesOfString:value withString:[NSString stringWithFormat:@"<%@ Obfuscated>", key]];
            }
        }
    }
    
    if (obfuscators && obfuscators.count > 0) {
        for (NSString *key in obfuscators) {
            NSString *value = [obfuscators objectForKey:key];
            
            if ([message contains:value]) {
//                @try {
//                    message = [message stringByReplacingOccurrencesOfString:value withString:[NSString stringWithFormat:@"<%@:%@>", key, [RSA encryptString:value publicKey:obfuscationKey]]];
//                } @catch (NSException *exception) {
                    message = [message stringByReplacingOccurrencesOfString:value withString:[NSString stringWithFormat:@"<%@ Obfuscated [OF]>", key]];
//                }
            }
        }
    }

    return message;
}

+ (void) e:(NSException *) exception {
    [LLog e:exception header:nil];
}

+ (void) e:(NSException *) exception header:(NSString *)header {
    if (header) LLog([LLog paddedForHeader:header PadTo:60]);
    
    LLog([NSString stringWithFormat:@"Exception: %@ - %@\n%@", exception.name, exception.reason, [exception callStackSymbols]]);
    //[Answers logCustomEventWithName:@"Non-Fatal Exception" customAttributes:@{@"ExceptionDescription":exception.description}];
}

+ (NSString *)paddedForHeader:(NSString *)padMe PadTo:(int)padTo {
    if (padMe.length + 4 >= padTo) {
        return [NSString stringWithFormat:@"- %@ -",padMe];
    } else {
        NSString *padWith = [@"" stringByPaddingToLength:floor((padTo - padMe.length - 2) / 2.0) withString:@"-" startingAtIndex:0];
        
        padMe = [NSString stringWithFormat:@"%@ %@ %@",padWith,padMe,padWith];
        if (padMe.length < padTo) padMe = [padMe stringByAppendingString:@"-"];
        return padMe;
    }
}

+ (bool) isFirstLaunch {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *num = [defaults valueForKey:klmPrefFirstTimeLaunch];
    BOOL isFirstLaunch = num ? [num boolValue]: true;

    if (isFirstLaunch) {
        [defaults setValue:[NSNumber numberWithBool:false] forKey:klmPrefFirstTimeLaunch];
        [defaults synchronize];
        return true;
    } else {
        return false;
    }
}

+ (NSString *) instanceID {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *iID = [defaults valueForKey:klmPrefInstanceID];
    
    if (!iID) {
        iID = [[NSUUID UUID] UUIDString];
        [defaults setValue:iID forKey:klmPrefInstanceID];
        [defaults synchronize];
    }
    
    return iID;
}

+ (void)printAppStartingInfo {
    LLog([@"" stringByPaddingToLength:60 withString:@"-" startingAtIndex:0]);
    LLog([self paddedForHeader:@"App Launched" PadTo:60]);
    if ([self isFirstLaunch]) LLog([self paddedForHeader:@"First Launch" PadTo:60]);
    NSString *versionInfoString = [NSString stringWithFormat:@"Version: %@ (%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    LLog([self paddedForHeader:versionInfoString PadTo:60]);
    LLog([self paddedForHeader:[[NSDate date] stringFormatted:@"EEE MMM dd HH:mm:ss zzz yyyy"] PadTo:60]);
    LLog([self paddedForHeader:[LLog instanceID] PadTo:60]);
    LLog([@"" stringByPaddingToLength:60 withString:@"-" startingAtIndex:0]);
}

- (void)setSanitizeBlock:(NSString *(^) (NSString *message))logSanitizeBlock {
    sanitizeLogOutputBlock = logSanitizeBlock;
}

- (void)log:(NSString *)format, ... {
    va_list ap;
    va_start(ap, format);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyMMdd HH:mm:ss SSS"];
    NSDate *tDate = [[NSDate date] copy];
    NSString *tFDate = [formatter stringFromDate:tDate];
    
    NSString *message = [[NSString alloc] initWithFormat:format arguments:ap];
    va_end(ap);

    message = [self cutOut:message];
    if (sanitizeLogOutputBlock) message = sanitizeLogOutputBlock(message); else message = [self sanitizeLog:message];
    
    NSLog(@"%@",message);
    //CLSNSLog(@"%@",message);
    
    message = [tFDate stringByAppendingFormat:@" | %@\n",message];
    [logFile writeData:[message dataUsingEncoding:NSUTF8StringEncoding]];
    [logFile synchronizeFile];
    
    //Keep the file under a half meg, truncating ~100k each time it hits ~512k
    unsigned long long fileL = [logFile seekToEndOfFile];
    if (fileL > 4096000) {
        [logFile seekToFileOffset:256000];
        NSData *tDat = [logFile readDataToEndOfFile];
        [logFile truncateFileAtOffset:0];
        [logFile writeData:tDat];
        [logFile synchronizeFile];
        tDat = nil;
    }
}

- (NSString *) cutOut:(NSString *)outThis {
    if (outThis.length > 20000)
        return @"-Log Line Exempt, 20000+ length-";
    else
        return outThis;
}

- (NSData *)debugFileData {
    [logFile seekToFileOffset:0];
    NSData *returnData = [logFile readDataToEndOfFile];
    return returnData;
}

+ (LLog *)sharedInstance {
    static LLog *instance = nil;
    if (instance == nil) instance = [[LLog alloc] init];
    return instance;
}

@end
