//  UITapGestureRecognizer+LMAdditions.m
//  Created by Louis Moga

#import "UITapGestureRecognizer+LMAdditions.h"
#import <objc/runtime.h>

@interface UITapGestureRecognizer (LMAdditions)
- (void)bk_handleAction:(UIGestureRecognizer *)recognizer;
- (void)bk_setHandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))handler;
@end


@implementation UITapGestureRecognizer (LMAdditions)


- (void)setLmTag2:(NSNumber *)_lmTag2 {
    objc_setAssociatedObject(self, @selector(lmTag2), _lmTag2, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)lmTag2 {
    NSNumber *returnVal = objc_getAssociatedObject(self, @selector(lmTag2));
    return returnVal ? returnVal : [NSNumber numberWithInt:0];
}


- (void)bk_setHandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))handler
{
    objc_setAssociatedObject(self, @"BKGestureRecognizerBlockKey", handler, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



- (void)setBkhandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))handler
{
    objc_setAssociatedObject(self, @"BKGestureRecognizerBlockKey", handler, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))bk_handler
{
    return objc_getAssociatedObject(self, @"BKGestureRecognizerBlockKey");
}




- (id)initWithHandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))block
{
    self = [self initWithTarget:self action:@selector(bk_handleAction:)];
    if (!self) return nil;

    [self setBkhandler:block];

    return self;
}


- (void)bk_handleAction:(UITapGestureRecognizer *)recognizer
{
    void (^handler)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) = recognizer.bk_handler;
    if (!handler) return;
    
    CGPoint location = [self locationInView:self.view];
    void (^block)(void) = ^{
        handler(self, self.state, location);
    };

    block();
}

@end
