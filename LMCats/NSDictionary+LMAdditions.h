//  NSDictionary+LMAdditions.h
//  Created by Louis Moga

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSDictionary (LMAdditions)


- (NSArray *)lmToSimpleArray;

@end
