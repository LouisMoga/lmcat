//  NSString+LMAdditions.m
//  Created by Louis Moga

#import "NSString+LMAdditions.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (LMAdditions)

- (BOOL)matchesRegex:(NSString *)regex {
    NSPredicate *regextest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [regextest evaluateWithObject:self];
}

- (NSString *)lmStringWithURLEncoding {
    return [[NSURL URLWithDataRepresentation:[self dataUsingEncoding:NSUTF8StringEncoding] relativeToURL:nil] relativeString];
}

- (NSString *)paddedForHeader:(int)padTo {
    if (self.length + 4 >= padTo) {
        return [NSString stringWithFormat:@"- %@ -", self];
    } else {
        NSString *padWith = [@"" stringByPaddingToLength:floor((padTo - self.length - 2) / 2.0) withString:@"-" startingAtIndex:0];
        
        NSString *padMeNew = [NSString stringWithFormat:@"%@ %@ %@",padWith, self, padWith];
        if (padMeNew.length < padTo) padMeNew = [padMeNew stringByAppendingString:@"-"];

        return padMeNew;
    }
}

- (NSString *)lmGetSHA1Hash {
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (uint)data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

- (NSString *)lmFormat10digitsToPhoneNumber {
    if (self.length != 10) return self;
    return [NSString stringWithFormat: @"(%@) %@-%@", [self substringWithRange:NSMakeRange(0,3)],[self substringWithRange:NSMakeRange(3,3)],
            [self substringWithRange:NSMakeRange(6,4)]];
}

- (CGFloat)lmHeightInFont:(UIFont *)theFont {
    return [self lmSizeInFont:theFont].height;
}

- (CGFloat)lmHeightInFont:(UIFont *)theFont inWidth:(double)theWidth {
    return  [self lmSizeInFont:theFont inWidth:theWidth].height;
}

- (CGSize)lmSizeInFont:(UIFont *)theFont {
    if (!theFont) return CGSizeZero;
    CGSize tS;
//    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//        #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
//        tS = [self sizeWithFont:theFont];
//    } else {
        tS = [(NSString *)self sizeWithAttributes: @{NSFontAttributeName: theFont}];
//    }
    
    return tS;
}

- (CGSize)lmSizeInFont:(UIFont *)theFont inWidth:(double)theWidth {
    if (!theFont) return CGSizeZero;
//    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//        #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
//        CGSize tS = [(NSString *)self sizeWithFont:theFont constrainedToSize:CGSizeMake(theWidth, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
//        return tS;
//    } else {
        CGRect tR = [
                     (NSString *)self boundingRectWithSize:CGSizeMake(theWidth, CGFLOAT_MAX)
                     options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                     attributes:@{NSFontAttributeName: theFont}
                     context:nil];
        return tR.size;
//    }
}

- (NSString *)lmAppendStringListFormatted:(NSArray *)theList {
    NSString *modifiedString = self;
    for (int i = 0; i < theList.count; i++) {
        if (i > 0 && i < (theList.count - 1) && theList.count > 2)
            modifiedString = [modifiedString stringByAppendingString:@", "];
        else if ((i == 1 && theList.count == 2) || (i == (theList.count - 1) && theList.count > 2))
            modifiedString = [modifiedString stringByAppendingString:@" and "];
        
        modifiedString = [modifiedString stringByAppendingString:[theList objectAtIndex:i]];
    }
    return modifiedString;
}

- (BOOL)contains:(NSString *)string {
    NSRange range = [self rangeOfString:string];
    return (range.location != NSNotFound);
}


//This function from http://stackoverflow.com/questions/938095/number-of-occurrences-of-a-character-in-nsstring
- (NSUInteger)occurrenceCountOfCharacter:(UniChar)character
{
    CFStringRef selfAsCFStr = (__bridge CFStringRef)self;
    
    CFStringInlineBuffer inlineBuffer;
    CFIndex length = CFStringGetLength(selfAsCFStr);
    CFStringInitInlineBuffer(selfAsCFStr, &inlineBuffer, CFRangeMake(0, length));
    
    NSUInteger counter = 0;
    
    for (CFIndex i = 0; i < length; i++) {
        UniChar c = CFStringGetCharacterFromInlineBuffer(&inlineBuffer, i);
        if (c == character) counter += 1;
    }
    
    return counter;
}













@end
