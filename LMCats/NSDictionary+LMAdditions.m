//  NSDictionary+LMAdditions.m
//  Created by Louis Moga

#import "NSDictionary+LMAdditions.h"

@implementation NSDictionary (LMAdditions)

- (NSArray *)lmToSimpleArray {
    NSMutableArray *output = [[NSMutableArray alloc] init];
    for (NSString *tKey in self.allKeys) {
        [output addObject:@[[self objectForKey:tKey],tKey]];
    }
    return output;
}

@end
