//
//  UIViewController+LMAdditions.h
//  LMCats
//
//  Created by Louis Moga on 11/9/16.
//  Copyright © 2016 Louis Moga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (LMAdditions)

- (float)lmFrameBottom;
- (float)lmWidth;
- (float)lmHeight;



- (void) lmRemoveHeightCon:(UIView *)view Priority:(int)priority;


- (void)lmConCenterX:(UIView *)thisItem;
- (void)lmConCenterY:(UIView *)thisItem;
- (void)lmConPlace:(UIView *)thisItem Below:(UIView*)belowThis By:(float)spacing WithHeight:(float)height;
- (void)lmConPlace:(UIView *)thisItem Above:(UIView*)aboveThis By:(float)spacing WithHeight:(float)height;

- (void)lmConPlace:(UIView *)thisItem Below:(UIView*)belowThis By:(float)spacing WithHeight:(float)height sideless:(BOOL)Sideless;
- (void)lmConPlace:(UIView *)thisItem Above:(UIView*)aboveThis By:(float)spacing WithHeight:(float)height sideless:(BOOL)Sideless;
- (void)lmConSetHeight:(UIView *)view setTo:(float)thisHeight;
- (void)lmConSetHeight:(UIView *)view setTo:(float)thisHeight Priority:(int)Priority;
- (void)lmConSetWidth:(UIView *)ofItem setTo:(float)thisWidth;

- (void)lmConSetWidthOf:(UIView *)ofItem ToWidthOf:(UIView *)matchItem WithRatio:(float)ratio;
- (void)lmConSetWidthOf:(UIView *)ofItem ToWidthOf:(UIView *)matchItem ConstraintParent:(UIView *)ConPar WithRatio:(float)ratio;
- (void)lmConSetWidthOf:(UIView *)ofItem ToWidthOf:(UIView *)matchItem ConstraintParent:(UIView *)ConPar WithRatio:(float)ratio WithAdjustment:(float)adjust;

- (void)lmConSetHeightOf:(UIView *)ofItem ToHeightOf:(UIView *)matchItem WithRatio:(float)ratio;
- (void)lmConSetHeightOf:(UIView *)ofItem ToHeightOf:(UIView *)matchItem ConstraintParent:(UIView *)ConPar WithRatio:(float)ratio;
- (void)lmConSetHeightOf:(UIView *)ofItem ToHeightOf:(UIView *)matchItem ConstraintParent:(UIView *)ConPar WithRatio:(float)ratio WithAdjustment:(float)adjust;

- (void)lmConTopOf:(UIView *)topItem toBottomOf:(UIView *)bottomItem;
- (void)lmConTopOf:(UIView *)topItem toBottomOf:(UIView *)bottomItem withPadding:(float)padding;
- (void)lmConBottomOf:(UIView *)bottomItem toTopOf:(UIView *)topItem;
- (void)lmConBottomOf:(UIView *)bottomItem toTopOf:(UIView *)topItem withPadding:(float)padding;
- (void)lmConLeftOf:(UIView *)leftItem toRightOf:(UIView *)rightItem;
- (void)lmConLeftOf:(UIView *)leftItem toRightOf:(UIView *)rightItem withPadding:(float)padding;
- (void)lmConRightOf:(UIView *)RightItem toLeftOf:(UIView *)leftItem;
- (void)lmConRightOf:(UIView *)rightItem toLeftOf:(UIView *)leftItem withPadding:(float)padding;
- (void)lmConSetAspectHeight:(UIView *)toThis withRatio:(float)theRatio;
- (void)lmConSetAspectWidth:(UIView *)toThis withRatio:(float)theRatio;
- (void)lmConAdd:(UIView *)toThis item:(UIView *)item attr:(NSLayoutAttribute)attr relation:(NSLayoutRelation)relation to:(UIView *)toView toAttr:(NSLayoutAttribute) toAttr val:(float) val;
- (NSLayoutConstraint *)lmConAdd:(UIView *)toThis item:(UIView *)item attr:(NSLayoutAttribute)attr relation:(NSLayoutRelation)relation to:(UIView *)toView toAttr:(NSLayoutAttribute) toAttr val:(float) val multiplier:(float)multi;
- (NSLayoutConstraint *)lmConAdd:(UIView *)toThis item:(UIView *)item attr:(NSLayoutAttribute)attr relation:(NSLayoutRelation)relation to:(UIView *)toView toAttr:(NSLayoutAttribute) toAttr val:(float) val multiplier:(float)multi priority:(int)priority;
- (void)lmConMatch:(UIView *)subView;
- (void)lmConMatch:(UIView *)subView MatchMargins:(BOOL)matchMargins;
- (void)lmConMatch:(UIView *)subView onSection:(NSLayoutAttribute)section;
- (void)lmConMatch:(UIView *)subView onSection:(NSLayoutAttribute)section withPadding:(float)padding;
- (void)lmConMatch:(UIView *)subView onSection:(NSLayoutAttribute)section To:(NSLayoutAttribute)matchAtt;
- (void)lmConMatchLeftRight:(UIView *)subView;
- (void)lmConMatchLeftRight:(UIView *)subView withPadding:(float)padding;
- (void)lmConMatchLeftRight:(UIView *)subView withPadding:(float)padding MatchMargins:(BOOL)matchMargins;
- (void)lmConMatchTopBottom:(UIView *)subView;
- (void)lmConMatchTopBottom:(UIView *)subView withPadding:(float)padding;
- (void)lmConMatchTopBottom:(UIView *)subView withPadding:(float)padding MatchMargins:(BOOL)matchMargins;



- (void) lmAdjustHeightCon:(UIView *)view adjustTo:(double)height;
- (void) lmRemoveHeightCon:(UIView *)view;



@end
