//
//  UIColor+UIColor_LMAdditions.h
//  LMCats
//
//  Created by Louis Moga on 1/9/17.
//  Copyright © 2017 Louis Moga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (LMAdditions)

- (UIColor *)darkerColor;
- (UIColor *)lighterColor;

@end
