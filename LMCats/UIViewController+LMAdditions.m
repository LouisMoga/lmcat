//
//  UIViewController+LMAdditions.m
//  LMCats
//
//  Created by Louis Moga on 11/9/16.
//  Copyright © 2016 Louis Moga. All rights reserved.
//

#import "UIViewController+LMAdditions.h"

@implementation UIViewController (LMAdditions)


- (float)lmFrameBottom {
    return self.view.frame.origin.y + self.view.frame.size.height;
}

- (float)lmWidth {
    return self.view.frame.size.width;
}

- (float)lmHeight {
    return self.view.frame.size.height;
}






- (void)lmConCenterX:(UIView *)thisItem {
    [self lmConMatch:thisItem onSection:NSLayoutAttributeCenterX withPadding:0];
}

- (void)lmConCenterY:(UIView *)thisItem {
    [self lmConMatch:thisItem onSection:NSLayoutAttributeCenterY withPadding:0];
}

- (void)lmConPlace:(UIView *)thisItem Below:(UIView*)belowThis By:(float)spacing WithHeight:(float)height {
    [self lmConPlace:thisItem Below:belowThis By:spacing WithHeight:height sideless:NO];
}

- (void)lmConPlace:(UIView *)thisItem Above:(UIView*)aboveThis By:(float)spacing WithHeight:(float)height {
    [self lmConPlace:thisItem Above:aboveThis By:spacing WithHeight:height sideless:NO];
}

- (void)lmConPlace:(UIView *)thisItem Below:(UIView*)belowThis By:(float)spacing WithHeight:(float)height sideless:(BOOL)Sideless {
    if (height > 0) [self lmConSetHeight:thisItem setTo:height];
    
    if (!belowThis || [belowThis isEqual:thisItem.superview])
        [self lmConAdd:thisItem.superview item:thisItem attr:NSLayoutAttributeTop relation:NSLayoutRelationEqual
                    to:thisItem.superview toAttr:NSLayoutAttributeTop val:spacing];
    else
        [self lmConAdd:thisItem.superview item:thisItem attr:NSLayoutAttributeTop relation:NSLayoutRelationEqual
                    to:belowThis toAttr:NSLayoutAttributeBottom val:spacing];
    
    if (!Sideless) [self lmConMatchLeftRight:thisItem withPadding:spacing];
}

- (void)lmConPlace:(UIView *)thisItem Above:(UIView*)aboveThis By:(float)spacing WithHeight:(float)height sideless:(BOOL)Sideless {
    if (height > 0) [self lmConSetHeight:thisItem setTo:height];
    
    if ([aboveThis isEqual:thisItem.superview])
        [self lmConAdd:thisItem.superview item:thisItem attr:NSLayoutAttributeBottom relation:NSLayoutRelationEqual
                    to:aboveThis toAttr:NSLayoutAttributeBottom val:-spacing];
    else
        [self lmConAdd:thisItem.superview item:thisItem attr:NSLayoutAttributeBottom relation:NSLayoutRelationEqual
                    to:aboveThis toAttr:NSLayoutAttributeTop val:-spacing];
    
    if (!Sideless) [self lmConMatchLeftRight:thisItem withPadding:spacing];
}


- (void)lmConSetHeight:(UIView *)view setTo:(float)thisHeight {
    [self lmConAdd:view item:view attr:NSLayoutAttributeHeight relation:NSLayoutRelationEqual to:nil toAttr:NSLayoutAttributeNotAnAttribute val:thisHeight];
}

- (void)lmConSetHeight:(UIView *)view setTo:(float)thisHeight Priority:(int)Priority {
    [self lmConAdd:view item:view attr:NSLayoutAttributeHeight relation:NSLayoutRelationEqual to:nil toAttr:NSLayoutAttributeNotAnAttribute val:thisHeight multiplier:1.0 priority:Priority];
}

- (void)lmConSetWidth:(UIView *)ofItem setTo:(float)thisWidth {
    [self lmConAdd:ofItem item:ofItem attr:NSLayoutAttributeWidth relation:NSLayoutRelationEqual to:nil toAttr:NSLayoutAttributeNotAnAttribute val:thisWidth];
}


- (void)lmConSetWidthOf:(UIView *)ofItem ToWidthOf:(UIView *)matchItem WithRatio:(float)ratio {
    [self lmConSetWidthOf:ofItem ToWidthOf:matchItem ConstraintParent:matchItem WithRatio:ratio];
}

- (void)lmConSetWidthOf:(UIView *)ofItem ToWidthOf:(UIView *)matchItem ConstraintParent:(UIView *)ConPar WithRatio:(float)ratio {
    [self lmConSetWidthOf:ofItem ToWidthOf:matchItem ConstraintParent:ConPar WithRatio:ratio WithAdjustment:0];
}

- (void)lmConSetWidthOf:(UIView *)ofItem ToWidthOf:(UIView *)matchItem ConstraintParent:(UIView *)ConPar WithRatio:(float)ratio WithAdjustment:(float)adjust {
    [self lmConAdd:ConPar item:ofItem attr:NSLayoutAttributeWidth relation:NSLayoutRelationEqual to:matchItem toAttr:NSLayoutAttributeWidth val:adjust multiplier:ratio];
}


- (void)lmConSetHeightOf:(UIView *)ofItem ToHeightOf:(UIView *)matchItem WithRatio:(float)ratio {
    [self lmConSetHeightOf:ofItem ToHeightOf:matchItem ConstraintParent:matchItem WithRatio:ratio];
}

- (void)lmConSetHeightOf:(UIView *)ofItem ToHeightOf:(UIView *)matchItem ConstraintParent:(UIView *)ConPar WithRatio:(float)ratio {
    [self lmConSetHeightOf:ofItem ToHeightOf:matchItem ConstraintParent:ConPar WithRatio:ratio WithAdjustment:0];
}

- (void)lmConSetHeightOf:(UIView *)ofItem ToHeightOf:(UIView *)matchItem ConstraintParent:(UIView *)ConPar WithRatio:(float)ratio WithAdjustment:(float)adjust {
    [self lmConAdd:ConPar item:ofItem attr:NSLayoutAttributeHeight relation:NSLayoutRelationEqual to:matchItem toAttr:NSLayoutAttributeHeight val:adjust multiplier:ratio];
}

- (void)lmConTopOf:(UIView *)topItem toBottomOf:(UIView *)bottomItem {
    [self lmConTopOf:topItem toBottomOf:bottomItem withPadding:0];
}

- (void)lmConTopOf:(UIView *)topItem toBottomOf:(UIView *)bottomItem withPadding:(float)padding {
    [self lmConAdd:topItem.superview item:topItem attr:NSLayoutAttributeTop relation:NSLayoutRelationEqual
                to:bottomItem toAttr:NSLayoutAttributeBottom val:padding];
}


- (void)lmConBottomOf:(UIView *)bottomItem toTopOf:(UIView *)topItem {
    [self lmConBottomOf:bottomItem toTopOf:topItem withPadding:0];
}

- (void)lmConBottomOf:(UIView *)bottomItem toTopOf:(UIView *)topItem withPadding:(float)padding {
    [self lmConAdd:bottomItem.superview item:bottomItem attr:NSLayoutAttributeBottom relation:NSLayoutRelationEqual
                to:topItem toAttr:NSLayoutAttributeTop val:padding];
}


- (void)lmConLeftOf:(UIView *)leftItem toRightOf:(UIView *)rightItem {
    [self lmConLeftOf:leftItem toRightOf:rightItem withPadding:0];
}

- (void)lmConLeftOf:(UIView *)leftItem toRightOf:(UIView *)rightItem withPadding:(float)padding {
    [self lmConAdd:leftItem.superview item:leftItem attr:NSLayoutAttributeLeft relation:NSLayoutRelationEqual
                to:rightItem toAttr:NSLayoutAttributeRight val:padding];
}


- (void)lmConRightOf:(UIView *)RightItem toLeftOf:(UIView *)leftItem {
    [self lmConRightOf:RightItem toLeftOf:leftItem withPadding:0];
}

- (void)lmConRightOf:(UIView *)rightItem toLeftOf:(UIView *)leftItem withPadding:(float)padding {
    [self lmConAdd:rightItem.superview item:rightItem attr:NSLayoutAttributeRight relation:NSLayoutRelationEqual
                to:leftItem toAttr:NSLayoutAttributeLeft val:padding];
}




- (void)lmConSetAspectHeight:(UIView *)toThis withRatio:(float)theRatio {
    [self lmConAdd:toThis item:toThis attr:NSLayoutAttributeHeight relation:NSLayoutRelationEqual to:toThis toAttr:NSLayoutAttributeWidth val:0 multiplier:theRatio];
}

- (void)lmConSetAspectWidth:(UIView *)toThis withRatio:(float)theRatio {
    [self lmConAdd:toThis item:toThis attr:NSLayoutAttributeWidth relation:NSLayoutRelationEqual to:toThis toAttr:NSLayoutAttributeHeight val:0 multiplier:theRatio];
}



- (void)lmConAdd:(UIView *)toThis item:(UIView *)item attr:(NSLayoutAttribute)attr relation:(NSLayoutRelation)relation to:(UIView *)toView toAttr:(NSLayoutAttribute) toAttr val:(float) val {
    [self lmConAdd:toThis item:item attr:attr relation:relation to:toView toAttr:toAttr val:val multiplier:1.0];
}

- (NSLayoutConstraint *)lmConAdd:(UIView *)toThis item:(UIView *)item attr:(NSLayoutAttribute)attr relation:(NSLayoutRelation)relation to:(UIView *)toView toAttr:(NSLayoutAttribute) toAttr val:(float) val multiplier:(float)multi {
    return [self lmConAdd:toThis item:item attr:attr relation:relation to:toView toAttr:toAttr val:val multiplier:multi priority:-1];
}

- (NSLayoutConstraint *)lmConAdd:(UIView *)toThis item:(UIView *)item attr:(NSLayoutAttribute)attr relation:(NSLayoutRelation)relation to:(UIView *)toView toAttr:(NSLayoutAttribute) toAttr val:(float) val multiplier:(float)multi priority:(int)priority {
    if (!toThis || !item || (!toView && toAttr != NSLayoutAttributeNotAnAttribute)) {
        NSLog(@"LM: Attempted to add invalid constraint!");
        return nil;
    } else {
        NSLayoutConstraint *t = [NSLayoutConstraint
                                 constraintWithItem: item
                                 attribute:attr
                                 relatedBy:relation
                                 toItem:toView
                                 attribute:toAttr
                                 multiplier:multi
                                 constant:val];
        if (priority >= 0) [t setPriority:priority];
        
        [toThis addConstraint:t];
        return t;
    }
}




- (void)lmConMatch:(UIView *)subView {
    [self lmConMatch:subView MatchMargins:YES];
}

- (void)lmConMatch:(UIView *)subView MatchMargins:(BOOL)matchMargins {
    [self lmConAdd:subView.superview item:subView attr:NSLayoutAttributeTop relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchMargins?NSLayoutAttributeTopMargin:NSLayoutAttributeTop val:0];
    
    
    [self lmConAdd:subView.superview item:subView attr:NSLayoutAttributeBottom relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchMargins?NSLayoutAttributeBottomMargin:NSLayoutAttributeBottom val:0];
    
    
    [self lmConAdd:subView.superview item:subView attr:NSLayoutAttributeLeft relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchMargins?NSLayoutAttributeLeftMargin:NSLayoutAttributeLeft val:0];
    
    
    [self lmConAdd:subView.superview item:subView attr:NSLayoutAttributeRight relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchMargins?NSLayoutAttributeRightMargin:NSLayoutAttributeRight val:0];
}


- (void)lmConMatch:(UIView *)subView onSection:(NSLayoutAttribute)section {
    [self lmConMatch:subView onSection:section withPadding:0];
}

- (void)lmConMatch:(UIView *)subView onSection:(NSLayoutAttribute)section withPadding:(float)padding {
    [self lmConAdd:subView.superview item:subView attr:section relation:NSLayoutRelationEqual
                to:subView.superview toAttr:section val:padding];
}

- (void)lmConMatch:(UIView *)subView onSection:(NSLayoutAttribute)section To:(NSLayoutAttribute)matchAtt {
    [self lmConAdd:subView.superview item:subView attr:section relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchAtt val:0];
}


- (void)lmConMatchLeftRight:(UIView *)subView {
    [self lmConMatchLeftRight:subView withPadding:0];
}

- (void)lmConMatchLeftRight:(UIView *)subView withPadding:(float)padding {
    [self lmConMatchLeftRight:subView withPadding:padding MatchMargins:YES];
}

- (void)lmConMatchLeftRight:(UIView *)subView withPadding:(float)padding MatchMargins:(BOOL)matchMargins {
    [self lmConAdd:subView.superview item:subView attr:NSLayoutAttributeLeft relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchMargins?NSLayoutAttributeLeftMargin:NSLayoutAttributeLeft val:padding];
    
    [self lmConAdd:subView.superview item:subView attr:NSLayoutAttributeRight relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchMargins?NSLayoutAttributeRightMargin:NSLayoutAttributeRight val:-padding];
}


- (void)lmConMatchTopBottom:(UIView *)subView {
    [self lmConMatchTopBottom:subView withPadding:0];
}

- (void)lmConMatchTopBottom:(UIView *)subView withPadding:(float)padding {
    [self lmConMatchTopBottom:subView withPadding:0 MatchMargins:YES];
}

- (void)lmConMatchTopBottom:(UIView *)subView withPadding:(float)padding MatchMargins:(BOOL)matchMargins {
    [self lmConAdd:subView.superview item:subView attr:NSLayoutAttributeTop relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchMargins?NSLayoutAttributeTopMargin:NSLayoutAttributeTop val:padding];
    
    [self lmConAdd:subView.superview item:subView attr:NSLayoutAttributeBottom relation:NSLayoutRelationEqual
                to:subView.superview toAttr:matchMargins?NSLayoutAttributeBottomMargin:NSLayoutAttributeBottom val:-padding];
}




- (void) lmAdjustHeightCon:(UIView *)view adjustTo:(double)height {
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstItem == view && constraint.firstAttribute == NSLayoutAttributeHeight) {
            constraint.constant = height;
        }
    }
}

- (void) lmRemoveHeightCon:(UIView *)view {
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            [view removeConstraint:constraint];
        }
    }
}

- (void) lmRemoveHeightCon:(UIView *)view Priority:(int)priority {
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight && constraint.priority == priority) {
            [view removeConstraint:constraint];
        }
    }
}






@end
