//  UITapGestureRecognizer+LMAdditions.h
//  Created by Louis Moga

#import <UIKit/UIKit.h>

@interface UITapGestureRecognizer (LMAdditions)

@property(nonatomic) NSNumber *lmTag2;

- (id)initWithHandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))block;

@end
