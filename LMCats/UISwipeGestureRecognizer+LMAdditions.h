//
//  UISwipeGestureRecognizer+LMAdditions.h
//  LMCats
//
//  Created by Louis Work on 10/14/21.
//  Copyright © 2021 Louis Moga. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UISwipeGestureRecognizer (LMAdditions)

- (id)initWithHandler:(void (^)(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location))block;

@end

NS_ASSUME_NONNULL_END
